module Rand001(main) where

import System.Random
import System.IO

tstRnd rng = checkRange rng (genRnd 50 rng)

genRnd n rng = take n (randomRs rng (mkStdGen 2))

checkRange (lo,hi) = all pred
  where
   pred
    | lo <= hi  = \ x -> x >= lo && x <= hi
    | otherwise = \ x -> x >= hi && x <= lo

main :: Handle -> Handle -> IO ()
main stdout _ = do
  hPrint stdout (tstRnd (1,5::Double))
  hPrint stdout (tstRnd (1,5::Int))
  hPrint stdout (tstRnd (10,54::Integer))
  hPrint stdout (tstRnd ((-6),2::Int))
  hPrint stdout (tstRnd (2,(-6)::Int))


diff --git a/ghc-tcplugins-extra.cabal b/ghc-tcplugins-extra.cabal
index 7e120cb..2222502 100644
--- a/ghc-tcplugins-extra.cabal
+++ b/ghc-tcplugins-extra.cabal
@@ -47,7 +47,7 @@ library
   ghc-options: -Wall
   build-depends:
       base >=4.8 && <5
-    , ghc >=7.10 && <9.8
+    , ghc >=7.10 && <9.10
   default-language: Haskell2010
   if impl(ghc >= 8.0.0)
     ghc-options: -Wcompat -Wincomplete-uni-patterns -Widentities -Wredundant-constraints
@@ -55,7 +55,7 @@ library
     ghc-options: -fhide-source-paths
   if flag(deverror)
     ghc-options: -Werror
-  if impl(ghc >= 9.4) && impl(ghc < 9.8)
+  if impl(ghc >= 9.4) && impl(ghc < 9.10)
     other-modules:
         GhcApi.Constraint
         GhcApi.Predicate
@@ -67,7 +67,7 @@ library
         src-ghc-tree-9.4
         src-ghc-9.4
     build-depends:
-        ghc >=9.4 && <9.8
+        ghc >=9.4 && <9.10
   if impl(ghc >= 9.2) && impl(ghc < 9.4)
     other-modules:
         GhcApi.Constraint
diff --git a/src-ghc-9.4/Internal/Constraint.hs b/src-ghc-9.4/Internal/Constraint.hs
index 411d1b4..1c0075f 100644
--- a/src-ghc-9.4/Internal/Constraint.hs
+++ b/src-ghc-9.4/Internal/Constraint.hs
@@ -1,3 +1,4 @@
+{-# LANGUAGE CPP #-}
 {-# LANGUAGE RecordWildCards #-}
 
 module Internal.Constraint (newGiven, flatToCt, mkSubst, overEvidencePredType) where
@@ -8,6 +9,9 @@ import GhcApi.Constraint
 
 import GHC.Tc.Utils.TcType (TcType)
 import GHC.Tc.Types.Constraint (QCInst(..))
+#if MIN_VERSION_ghc(9,8,0)
+import GHC.Tc.Types.Constraint (DictCt(..), IrredCt(..), EqCt(..))
+#endif
 import GHC.Tc.Types.Evidence (EvTerm(..), EvBindsVar)
 import GHC.Tc.Plugin (TcPluginM)
 import qualified GHC.Tc.Plugin as TcPlugin (newGiven)
@@ -30,9 +34,15 @@ flatToCt _ = Nothing
 
 -- | Create simple substitution from type equalities
 mkSubst :: Ct -> Maybe ((TcTyVar, TcType),Ct)
+#if MIN_VERSION_ghc(9,8,0)
+mkSubst ct@(CEqCan (EqCt {..}))
+  | TyVarLHS tyvar <- eq_lhs
+  = Just ((tyvar,eq_rhs),ct)
+#else
 mkSubst ct@(CEqCan {..})
   | TyVarLHS tyvar <- cc_lhs
   = Just ((tyvar,cc_rhs),ct)
+#endif
 mkSubst _ = Nothing
 
 -- | Modify the predicate type of the evidence term of a constraint
@@ -42,8 +52,31 @@ overEvidencePredType f (CQuantCan qci) =
     ev :: CtEvidence
     ev = qci_ev qci
   in CQuantCan ( qci { qci_ev = ev { ctev_pred = f (ctev_pred ev) } } )
+#if MIN_VERSION_ghc(9,8,0)
+overEvidencePredType f (CDictCan di) =
+  let
+    ev :: CtEvidence
+    ev = di_ev di
+  in CDictCan ( di { di_ev = ev { ctev_pred = f (ctev_pred ev) } } )
+overEvidencePredType f (CIrredCan ir) =
+  let
+    ev :: CtEvidence
+    ev = ir_ev ir
+  in CIrredCan ( ir { ir_ev = ev { ctev_pred = f (ctev_pred ev) } } )
+overEvidencePredType f (CEqCan eq) =
+  let
+    ev :: CtEvidence
+    ev = eq_ev eq
+  in CEqCan ( eq { eq_ev = ev { ctev_pred = f (ctev_pred ev) } } )
+overEvidencePredType f (CNonCanonical ct) =
+  let
+    ev :: CtEvidence
+    ev = ct
+  in CNonCanonical ( ev { ctev_pred = f (ctev_pred ev) } )
+#else
 overEvidencePredType f ct =
   let
     ev :: CtEvidence
     ev = cc_ev ct
   in ct { cc_ev = ev { ctev_pred = f (ctev_pred ev) } }
+#endif
